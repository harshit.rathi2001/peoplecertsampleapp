package com.peoplecertsampleapp_master.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.peoplecertsampleapp_master.R;
import com.peoplecertsampleapp_master.pojo.DataModel;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ListViewHolder> {

    Context mCtx;
    List<DataModel> dataModelList;

    public RecyclerViewAdapter(Context mCtx, List<DataModel> dataModelList) {
        this.mCtx = mCtx;
        this.dataModelList = dataModelList;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_layout, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        DataModel dataModel = dataModelList.get(position);

        if (dataModel.getImageHref() != null) {
            Glide.with(mCtx)
                    .load(dataModel.getImageHref())
                    .centerCrop()
                    .apply(new RequestOptions().override(250, 250))
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .fallback(R.mipmap.place_holder)
                    .into(holder.imageView);

        }

        holder.textTitle.setText(dataModel.getTitle());
        holder.textDes.setText(dataModel.getDescription());
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textTitle, textDes;

        public ListViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            textTitle = itemView.findViewById(R.id.txt_title);
            textDes = itemView.findViewById(R.id.txt_des);
        }
    }
}
