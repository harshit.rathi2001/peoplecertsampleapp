package com.peoplecertsampleapp_master.activity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.peoplecertsampleapp_master.R;
import com.peoplecertsampleapp_master.adapter.RecyclerViewAdapter;
import com.peoplecertsampleapp_master.networkmanager.CheckConnection;
import com.peoplecertsampleapp_master.pojo.RootDataModel;
import com.peoplecertsampleapp_master.viewmodel.ContentViewModel;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("");
        setContentView(R.layout.activity_main);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        fetchTimelineAsync();

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code here
                fetchTimelineAsync();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Stop animation (This will be after 3 seconds) or can stop animation after api response
                        swipeContainer.setRefreshing(false);
                    }
                }, 4000); // Delay in millis
            }
        });

        // Scheme colors for animation
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void fetchTimelineAsync() {
        ContentViewModel model = new ViewModelProvider(this).get(ContentViewModel.class);
        if (CheckConnection.isConnection(MainActivity.this)) {
            model.getData().observe(this, (RootDataModel dataModel) -> {
                getSupportActionBar().setTitle(dataModel.getTitle());
                adapter = new RecyclerViewAdapter(MainActivity.this, dataModel.getRows());
                recyclerView.setAdapter(adapter);
            });
        } else {
            getSupportActionBar().setTitle("No network connection!");
            Toast.makeText(this, "Network is not available!", Toast.LENGTH_SHORT).show();
        }
    }
}
